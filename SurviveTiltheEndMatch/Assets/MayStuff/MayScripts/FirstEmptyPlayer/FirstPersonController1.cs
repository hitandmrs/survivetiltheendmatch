﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(CharacterController))]

public class FirstPersonController1 : MonoBehaviour {

    public float movementSpeed = 5.0f;

    public float mouseSensitivity = 5.0f;

    //up/down
    float verticalRotation = 0;
    public float upDownRange = 60.0f;

             //FOR JUMPING!
    float verticalVelocity = 0;
    public float jumpSpeed = 20.0f;

    CharacterController characterController;


    // Use this for initialization
    void Start () {
        Screen.lockCursor = false;

        //ground character controller
        characterController = GetComponent<CharacterController>();
    }
	
	// Update is called once per frame
	void Update ()
    {

        //ROTATION
        float rotLeftRight = Input.GetAxisRaw("Mouse X") * mouseSensitivity;
        transform.Rotate(0, rotLeftRight, 0);

        //UPDOWN
        //probably crashing because of the cameracontroller? 
        //not sure though?
        /*verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        verticalRotation = Mathf.Clamp(verticalRotation, -upDownRange, upDownRange);
        Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);*/

        //THIS BROKE MY CODE for updown shit!! TT
        /*float rotUpDown = Input.GetAxis("Mouse Y") * mouseSensitivity;
        float currentUpDown = Camera.main.transform.rotation.eulerAngles.x;
        float desiredUpDown = currentUpDown - rotUpDown;
        desiredUpDown = Mathf.Clamp(desiredUpDown, -upDownRange, upDownRange);
        Camera.main.transform.localRotation = Quaternion.Euler(desiredUpDown, 0, 0);*/

        //THIS BROKE MY CODE for up down shit!! TT
        /*float rotUpDown = Input.GetAxis("Mouse Y") * mouseSensitivity;
        float currentUpDown = Camera.main.transform.rotation.eulerAngles.x;
        Camera.main.transform.Rotate(rotUpDown, 0, 0);*/


        //MOVEMENT
        float forwardSpeed = Input.GetAxis("Vertical") * movementSpeed;
        float sideSpeed = Input.GetAxis("Horizontal") * movementSpeed;
        //X --> up/down
        //Y --> left/right
        //Z --> forward/backwards

        verticalVelocity += Physics.gravity.y * Time.deltaTime;         //Acceleration!

        //JUMP!!
        if(characterController.isGrounded && Input.GetButtonDown("Jump"))
        {
            verticalVelocity = jumpSpeed;
        }

        Vector3 speed = new Vector3(sideSpeed, verticalVelocity, forwardSpeed);

        speed = transform.rotation * speed;

        //cc.SimpleMove(speed);

        characterController.Move(speed * Time.deltaTime);     //expects how many units of distance you are traveling at certain frame (Time.deltaTime)

    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }
}
