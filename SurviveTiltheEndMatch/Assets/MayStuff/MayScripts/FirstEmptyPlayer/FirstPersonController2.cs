﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(CharacterController))]

public class FirstPersonController2 : MonoBehaviour {

    public float movementSpeed2 = 5.0f;

    public float mouseSensitivity2 = 5.0f;

    //up/down
    float verticalRotation2 = 0;
    public float upDownRange2 = 60.0f;

             //FOR JUMPING!
    float verticalVelocity2 = 0;
    public float jumpSpeed2 = 20.0f;

    CharacterController characterController2;

    // Use this for initialization
    void Start () {
        Screen.lockCursor = false;

        //ground character controller
        characterController2 = GetComponent<CharacterController>();
    }
	
	// Update is called once per frame
	void Update ()
    {

        //ROTATION
        float rotLeftRight2 = Input.GetAxisRaw("2Mouse X") * mouseSensitivity2;
        transform.Rotate(0, rotLeftRight2, 0);

        //UPDOWN
        //probably crashing because of the cameracontroller? 
        //not sure though?
        /*verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        verticalRotation = Mathf.Clamp(verticalRotation, -upDownRange, upDownRange);
        Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);*/

        //THIS BROKE MY CODE for updown shit!! TT
        /*float rotUpDown = Input.GetAxis("Mouse Y") * mouseSensitivity;
        float currentUpDown = Camera.main.transform.rotation.eulerAngles.x;
        float desiredUpDown = currentUpDown - rotUpDown;
        desiredUpDown = Mathf.Clamp(desiredUpDown, -upDownRange, upDownRange);
        Camera.main.transform.localRotation = Quaternion.Euler(desiredUpDown, 0, 0);*/

        //THIS BROKE MY CODE for up down shit!! TT
        /*float rotUpDown = Input.GetAxis("Mouse Y") * mouseSensitivity;
        float currentUpDown = Camera.main.transform.rotation.eulerAngles.x;
        Camera.main.transform.Rotate(rotUpDown, 0, 0);*/


        //MOVEMENT
        float forwardSpeed = Input.GetAxis("Vertical2") * movementSpeed2;
        float sideSpeed = Input.GetAxis("Horizontal2") * movementSpeed2;

       // float sideSpeed = Input.GetAxisRaw("Horizontal2") * movementSpeed2;

        //X --> up/down
        //Y --> left/right
        //Z --> forward/backwards

        verticalVelocity2 += Physics.gravity.y * Time.deltaTime;         //Acceleration!

            //JUMP!!
        if(characterController2.isGrounded && Input.GetButtonDown("Jump2"))
        {
            verticalVelocity2 = jumpSpeed2;
        }

        Vector3 speed = new Vector3(sideSpeed, verticalVelocity2, forwardSpeed);

        speed = transform.rotation * speed;

        //cc.SimpleMove(speed);

        characterController2.Move(speed * Time.deltaTime);     //expects how many units of distance you are traveling at certain frame (Time.deltaTime)

    }

    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }
}
