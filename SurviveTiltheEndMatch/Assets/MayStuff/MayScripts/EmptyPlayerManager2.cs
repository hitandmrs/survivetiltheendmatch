﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class EmptyPlayerManager2 : MonoBehaviour {
    public static int healthh = 100;

    //public static int healthh = 100;

    public GameObject player2;
    public Slider healthbar2;

    public GameObject menuContainer2;

    //LOAD HOMEPAGE!
    public string levelToLoad2;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("ReduceHealths", 1, 1);
    }

    void ReduceHealths()
    {
        healthh = healthh - 3;

        //healthh = healthh - 1;

        healthbar2.value = healthh;
       // healthbar.value = healthh;

        if (healthh <= 0)
        {
            menuContainer2.SetActive(true);
            healthh = 100;
            // player.GetComponent<Animator>().SetTrigger("isDead");
        }

       /* if (healthh <= 0)
        {
            menuContainer.SetActive(true);
            // player.GetComponent<Animator>().SetTrigger("isDead");
        }*/

    }

    // Update is called once per frame
    void Update()
    {
       // ReloadingLevel();
        GoToMainMenu();
    }

  /*  void ReloadingLevel()
    {
        if(Input.GetKey(KeyCode.O))
        {
            Application.LoadLevel(Application.loadedLevel); //press O on keyboard
           // SceneManager.LoadScene(SceneManager.GetActiveScene().name);

        }
    }*/

    void GoToMainMenu()
    {
        if (Input.GetButtonDown("Start2"))
        {
            SceneManager.LoadScene(levelToLoad2);    //press C to load homepage!
        }
    }
}
