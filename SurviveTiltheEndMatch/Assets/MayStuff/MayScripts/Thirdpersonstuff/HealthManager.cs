﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour {

    public int maxHealth;
    public int currentHealth;

    public PlayerController thePlayer;
    //public CameraController thePlayer;

    //FOR INVINCIBILITY!
    public float invincibilityLength;
    private float invincibilityCounter;

    //turn on and off the player when hit object
    public Renderer playerRenderer;

    public float flashCounter;
    public float flashLength = 0.1f;

	// Use this for initialization
	void Start () {

        currentHealth = maxHealth;

        //as soon as game starts so GM knows where player is!
        thePlayer = FindObjectOfType<PlayerController>();
        //thePlayer = FindObjectOfType<CameraController>();
	}
	
	// Update is called once per frame
	void Update () {
		
        if(invincibilityCounter > 0)
        {
            invincibilityCounter -= Time.deltaTime;

            flashCounter -= Time.deltaTime; //count down

            if(flashCounter <= 0)
            {
                playerRenderer.enabled = !playerRenderer.enabled;
                flashCounter = flashLength;
            }
            
            if(invincibilityCounter <= 0)
            {
                playerRenderer.enabled = true;
            }
        }
	}

    //this function should DAMAGE the player!
    public void HurtPlayer(int damage, Vector3 direction)
    {
        if (invincibilityCounter <= 0)
        {
            currentHealth -= damage;

            //need to knockback player and direction
            thePlayer.Knockback(direction);

            //players hurt
            invincibilityCounter = invincibilityLength;

            //hit turn off playerRenderer
            playerRenderer.enabled = false;

            flashCounter = flashLength;
        }
    }

    //this function HEALS player!
    public void HealPlayer(int healAmount)
    {
        currentHealth += healAmount;

        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
    }
}
