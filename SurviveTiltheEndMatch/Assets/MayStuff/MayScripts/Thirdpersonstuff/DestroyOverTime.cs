﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOverTime : MonoBehaviour {

    public float lifetime;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        //countdown lifetime for counter in game!
      /*  lifetime -= Time.deltaTime;
        if(lifetime < 0)
        {
            Destroy(gameObject);
        }*/

        Destroy(gameObject, lifetime); //destroy gameobject when lifetime runs out & starts counting down!
	}
}
