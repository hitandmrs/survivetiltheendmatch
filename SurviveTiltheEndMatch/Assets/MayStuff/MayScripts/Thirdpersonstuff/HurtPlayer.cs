﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayer : MonoBehaviour {

    //how much damage should give

    public int damageToGive = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //checks for any object that enter the trigger box that have this HURTPLAYER SCRIPT TO IT!
    private void OnTriggerEnter(Collider other)
    {
        //check to see which object has entered the trigger area
        //IS IT PLAYER? player.tag!

        if(other.gameObject.tag == "Player")
        {
            //SO player will get hit and object will determine what 

            //finding direction of the player
            Vector3 hitDirection = other.transform.position - transform.position;

            //normalize restricts player to be in a straight lined distance 
            //ensurring the basic knockback
            hitDirection = hitDirection.normalized;

            //hit player ==> damage
            FindObjectOfType<HealthManager>().HurtPlayer(damageToGive, hitDirection);
        }
    }
}
