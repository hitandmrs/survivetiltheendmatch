﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public Transform target;    //position, rotate, scale
    public Vector3 offset;      //camera follow player

    public bool useOffsetValues;    //Whether we will use offset values or not

    public float rotateSpeed; //camera looking at target & it rotates the camera the same way player is facing

    public Transform pivot;

    public float maxViewAngle;
    public float minViewAngle;

    public bool invertY;

    // Use this for initialization
    void Start ()
    {
        if (!useOffsetValues)       //if usedoffsetvalues is false then use the offset values otherwise use default values
        {
            offset = target.position - transform.position;      //wherever the target position is minus where the current transform position
        }

        pivot.transform.position = target.transform.position;       //pivot the target
                                                                    //pivot.transform.parent = target.transform;    //no longer want pivot to be the child of target
                                                                    //want camera to stay facign one direction and move player in diff direction

        pivot.transform.parent = null; //do this so that pivot is not a child of camera! && no more spinning of camera

        Cursor.lockState = CursorLockMode.Locked;           //hide the cursor
	}
	
	// LateUpdate is called once per frame --> happens after update
	void LateUpdate ()
    {
        pivot.transform.position = target.transform.position;   //now the pivot point will always move with the player
        
        //GET THE X POSITION OF THE MOUSE & ROTATE THE TARGET
        float horizontal = Input.GetAxis("Mouse X") * rotateSpeed; //MouseX --> moving left to right
        pivot.Rotate(0, horizontal, 0);

        //Get the Y POSITION OF THE MOUSE AND TO ROTATE THE PIVOT
        float vertical = Input.GetAxis("Mouse Y") * rotateSpeed;
       //ivot.Rotate(-vertical, 0, 0);             //player look down --> camera looks down, player looks up --> camera looks up

        if(invertY)
        {
            pivot.Rotate(vertical, 0, 0);
        }
        else
        {
            pivot.Rotate(-vertical, 0, 0);
        }
        //LIMITING THE UP/DOWN CAMERA ROTATION
        if(pivot.rotation.eulerAngles.x > maxViewAngle && pivot.rotation.eulerAngles.x < 180)
        {
            pivot.rotation = Quaternion.Euler(maxViewAngle, 0, 0);
        }

        //setting bottom value
        if(pivot.rotation.eulerAngles.x > 180f && pivot.rotation.eulerAngles.x < 360f + minViewAngle)
        {
            pivot.rotation = Quaternion.Euler(360f + minViewAngle, 0, 0);
        }

        //Move the camera nased on the current rotation of the target & the original offset
        float desiredYAngle = pivot.eulerAngles.y;     //eulerAngles --> vector3 object ACTUALLY 4 (x,y,z,w)
                                                        //TRANSFORM THE EULER ANGLES INTO VECTOR3 --> something simplier --> stored in desiredYAngle
        float desiredXAngle = pivot.eulerAngles.x;

        //New rotation applied to the CAMERA
        Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0);
        transform.position = target.position - (rotation * offset);     //wherever the current transform position is, set equal to the target position --> to give position that we want

        //transform.position = target.position - offset; //make camera move wherever player move

        if(transform.position.y < target.position.y)
        {
            transform.position = new Vector3(transform.position.x, target.position.y - 0.5f, transform.position.z);
        }

        transform.LookAt(target);      //takes the current transform (camera) & makes object rotate around within world to make look at target object 

    }
}
