﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class EmptyPlayerManager : MonoBehaviour {

    public static int health = 100;
    public GameObject player;
    public Slider healthbar;

    public GameObject menuContainer;

    public string levelToLoad;
    //public static Text plus15;

    // Use this for initialization
    void Start () {
        InvokeRepeating("ReduceHealth", 1, 1);
	}
	
    void ReduceHealth()
    {
        health = health - 3;

        healthbar.value = health;

                //no more game over screen when health --> 0
        if(health <= 0)
        {
            menuContainer.SetActive(true);
            health = 100; 
           // player.GetComponent<Animator>().SetTrigger("isDead");
        }
    }


    // Update is called once per frame
    void Update () {
      //  ReloadLevel();
       GoMainMenu();
       // InvokeRepeating("ReduceHealth", 1, 1);
        //plus15.text = "+15";
    }

    /*void ReloadLevel()
    {
        if (Input.GetKey(KeyCode.R))
        {
            //Application.LoadLevel(Application.loadedLevel); //press R on Keyboard
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }*/

    void GoMainMenu()     //level to load --> Level2
    {
        if (Input.GetButtonDown("Start"))
        {
           // Screen.lockCursor = true;
            SceneManager.LoadScene(levelToLoad);
          //  SceneManager.LoadScene(SceneManager.GetActiveScene().name);

            //Application.LoadLevel(Application.loadedLevel); //press M on Keyboard
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }


    /*public void MyMainMenu()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }*/

}
