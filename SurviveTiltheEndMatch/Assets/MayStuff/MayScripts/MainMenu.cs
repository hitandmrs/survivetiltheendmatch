﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

     public string LoadingSingleLvlOne;
     public string LoadingMultiLvlOne;

    void Update()
    {
        if (Input.GetButtonDown("Start"))
        {
            StartCoroutine(LoadSingleLvl1(LoadingSingleLvlOne));
        }

        else if (Input.GetButtonDown("Select"))
        {
            StartCoroutine(LoadMultiLvl1(LoadingMultiLvlOne));
        }

        else if (Input.GetButtonDown("Jump"))
        {
            QuitGame(); 
        }
    }

        IEnumerator LoadSingleLvl1(string singleName)
        {
           AsyncOperation singleop = SceneManager.LoadSceneAsync(singleName);
           yield return null;
           Debug.Log("Loading Single Player Game");
        }

        IEnumerator LoadMultiLvl1(string multiName)
        {
            AsyncOperation multiop = SceneManager.LoadSceneAsync(multiName);
            yield return null;
            Debug.Log("Loading Multi Player Game");
        }

    public void QuitGame()
        {
            Debug.Log("QUIT!");
            Application.Quit();
        }
}
