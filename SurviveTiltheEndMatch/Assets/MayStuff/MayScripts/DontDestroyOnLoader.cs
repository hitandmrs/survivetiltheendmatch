﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoader : MonoBehaviour {

    public bool isSingleton = true;
    public static DontDestroyOnLoader thisSingleObject = null;

	// Use this for initialization
	void Start () {

        DontDestroyOnLoad(gameObject);
        if(isSingleton)
        {
            if(thisSingleObject == null)
            {
                thisSingleObject = this;
            }
            
            else if (thisSingleObject != this)
            {
                Destroy(this);
            }
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
