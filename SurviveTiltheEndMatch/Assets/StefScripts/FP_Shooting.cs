﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FP_Shooting : MonoBehaviour
{
    public GameObject bullet;
    float bulletImpulse = 100f;
    public Camera player1cam; 


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Fire1"))
        { 
            GameObject thebullet = (GameObject)Instantiate(bullet, player1cam.transform.position + player1cam.transform.forward, player1cam.transform.rotation);
            thebullet.GetComponent<Rigidbody>().AddForce(player1cam.transform.forward * bulletImpulse, ForceMode.Impulse);
        }
    }
}
