﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement; 
using UnityEngine.UI; 

public class MultiManager : MonoBehaviour
{
    public string LoadingSceneName; 

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Select"))
        {
            StartCoroutine(LoadNewScene(LoadingSceneName));
        }
       
    }

    IEnumerator LoadNewScene(string sceneName)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        yield return null; 
    }
}
