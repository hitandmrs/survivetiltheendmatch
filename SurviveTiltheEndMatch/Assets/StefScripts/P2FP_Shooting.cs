﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2FP_Shooting : MonoBehaviour
{
    public GameObject bullet;
    float player2bulletImpulse = 100f;
    public Camera P2Camera; 


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("P2Fire1"))
        {
            GameObject player2thebullet = (GameObject)Instantiate(bullet, P2Camera.transform.position + P2Camera.transform.forward, P2Camera.transform.rotation);
            player2thebullet.GetComponent<Rigidbody>().AddForce(P2Camera.transform.forward * player2bulletImpulse, ForceMode.Impulse);

        }
    }
}
