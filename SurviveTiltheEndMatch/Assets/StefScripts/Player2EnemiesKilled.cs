﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class Player2EnemiesKilled : MonoBehaviour
{
    public static int P2userScore = 0;
    public Text Player2score;
    public CharacterController Player2; 

    // Start is called before the first frame update
    void Start()
    {
        Player2score = GetComponent<Text>();
        P2userScore = 0;
    }

    // Update is called once per frame
    void Update()
    {
            Player2score.text = "Germs Annihilated: " + P2userScore;
    }

}
