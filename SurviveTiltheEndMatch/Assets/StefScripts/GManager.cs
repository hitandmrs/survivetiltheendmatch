﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems; 
using UnityEngine.SceneManagement;

public class GManager : MonoBehaviour
{
   
    public void SinglePlayerGame()
    {
        SceneManager.LoadScene("Level2Single");
    }

    public void MultiPlayerGame()
    {
        SceneManager.LoadScene("Level2Multi");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
