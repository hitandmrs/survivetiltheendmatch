﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Bullet_Script : MonoBehaviour
{

    float player2lifespan = 3.0f;
    float player2germlifespan = 1.0f;
    float player2firelifespan = 3.0f; 
    public GameObject player2fireEffect; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        player2lifespan -= Time.deltaTime;
        player2germlifespan -= Time.deltaTime;
        player2firelifespan -= Time.deltaTime;


        if (player2lifespan <=0)
        {
            Explode(); 
        }

        if (player2germlifespan <= 0)
        {
            Destroy(GameObject.FindWithTag("Dead"));
        }

        if (player2firelifespan <= 0)
        {
            Destroy(GameObject.FindWithTag("Fiyah"));
        }
    }

    void OnCollisionEnter(Collision p2collision)
    {
        if (p2collision.gameObject.tag == "Enemy")
        {
            p2collision.gameObject.tag = "Dead";
            Instantiate(player2fireEffect, p2collision.transform.position, Quaternion.identity);
            Destroy(gameObject);
            Player2EnemiesKilled.P2userScore += 1;
        }
    }

    void Explode()
    {
        Destroy(gameObject);
    }
}
