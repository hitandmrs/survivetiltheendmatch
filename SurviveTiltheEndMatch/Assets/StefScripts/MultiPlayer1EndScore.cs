﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class MultiPlayer1EndScore : MonoBehaviour
{
    public static int PlayerOneEndScore;
    public Text FirstPlayerEndScore; 

    // Start is called before the first frame update
    void Start()
    {
        FirstPlayerEndScore = GetComponent<Text>(); 
    }

    // Update is called once per frame
    void Update()
    {
        FirstPlayerEndScore.text = Player1EnemiesKilled.P1userScore.ToString(); 
    }
}
