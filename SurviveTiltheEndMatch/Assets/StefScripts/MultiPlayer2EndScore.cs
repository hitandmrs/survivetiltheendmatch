﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class MultiPlayer2EndScore : MonoBehaviour
{
    public static int PlayerTwoEndScore;
    public Text SecondPlayerEndScore; 

    // Start is called before the first frame update
    void Start()
    {
        SecondPlayerEndScore = GetComponent<Text>(); 
    }

    // Update is called once per frame
    void Update()
    {
        SecondPlayerEndScore.text = Player2EnemiesKilled.P2userScore.ToString();    
    }
}
