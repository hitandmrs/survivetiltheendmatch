﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(CharacterController))] 


public class FirstPersonControllerPlayer2 : MonoBehaviour
{
    public float movementSpeed = 5.0f;
    public float mouseSensitivity = 2.0f;

    float verticalRotation = 0; 
    public float upDownRange = 60.0f;

    float verticalVelocity = 0;
    public float jumpSpeed = 0.25f;

    CharacterController characterController;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        // camera panning using mouse 
        float rotLeftRight = Input.GetAxisRaw("2Mouse X") * mouseSensitivity;
        transform.Rotate(0, rotLeftRight, 0);

        //verticalRotation -= Input.GetAxis("Mouse Y") * mouseSensitivity;
        //verticalRotation = Mathf.Clamp(verticalRotation, -upDownRange, upDownRange);
        //Camera.main.transform.localRotation = Quaternion.Euler(verticalRotation, 0, 0);


        // character movement using keys  
        float forwardSpeed = Input.GetAxis("Vertical2") * movementSpeed;
        float sideSpeed = Input.GetAxis("Horizontal2") * movementSpeed;

        verticalVelocity += Physics.gravity.y * Time.deltaTime;

        if ( characterController.isGrounded && Input.GetButton("Jump2"))
        {
            verticalVelocity = jumpSpeed; 
        }

        Vector3 speed = new Vector3(sideSpeed, verticalVelocity, forwardSpeed); // MOVEMENT: left right, up down, backwards forwards 

        speed = transform.rotation * speed; 

        characterController.Move(speed * Time.deltaTime); 
        
    }
}
