﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class SinglePlayerEndScore : MonoBehaviour
{
    public static int UserEndScore;
    public Text EndScore; 

    void Start()
    {
        EndScore = GetComponent<Text>();
    }

    private void Update()
    {
        EndScore.text = Player1EnemiesKilled.P1userScore.ToString();   
    }
}
