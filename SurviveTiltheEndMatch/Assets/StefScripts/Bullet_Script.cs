﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_Script : MonoBehaviour
{

    float lifespan = 3.0f;
    float germlifespan = 1.0f;
    float firelifespan = 3.0f; 
    public GameObject fireEffect; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        lifespan -= Time.deltaTime;
        germlifespan -= Time.deltaTime;
        firelifespan -= Time.deltaTime; 

        if (lifespan <=0)
        {
            Explode(); 
        }

        if (germlifespan <= 0)
        {
            Destroy(GameObject.FindWithTag("Dead"));
        }

        if (firelifespan <= 0)
        {
            Destroy(GameObject.FindWithTag("Fiyah"));
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            collision.gameObject.tag = "Dead";
            Instantiate(fireEffect, collision.transform.position, Quaternion.identity);
            Destroy(gameObject);
            Player1EnemiesKilled.P1userScore += 1;
        }
    }

    void Explode()
    {
        Destroy(gameObject);
    }

}
