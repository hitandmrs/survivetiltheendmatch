﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class Player1EnemiesKilled : MonoBehaviour
{
    public static int P1userScore = 0;
    public Text Player1score;
    public CharacterController Player1; 

    // Start is called before the first frame update
    void Start()
    {
        Player1score = GetComponent<Text>();
        P1userScore = 0; 
    }

    // Update is called once per frame
    void Update()
    {
            Player1score.text = "Germs Annihilated: " + P1userScore;
    }

}
