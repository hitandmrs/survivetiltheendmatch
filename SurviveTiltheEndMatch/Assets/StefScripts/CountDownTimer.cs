﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class CountDownTimer : MonoBehaviour
{

    float currentTime = 0;
    public static int startingTime = 80;
    public string sceneToLoad;
    public GameObject donezo; 

   [SerializeField] Text countDown; 

    // Start is called before the first frame update
    void Start()
    {
        countDown = GetComponent<Text>(); 
        currentTime = startingTime;
    }

    // Update is called once per frame
    void Update()
    {
        currentTime -= 1 * Time.deltaTime; // decreases time by one each second  
        countDown.text = currentTime.ToString("0"); 

        if (currentTime <= 0)
        {
            currentTime = 0;
            donezo.SetActive(true); 
            //Application.LoadLevel(sceneToLoad);
            //Cursor.visible = true;
        }
    }
}
