README.txt


Maytinee Apiwansri: 010433631

Regine Firmeza: 010524592 

Stefanie Lau: 010363418



SurviveTiltheEndMatch


"Survive Til the End Match" is a 3D STEM based game created by Maytinee Apiwansri, Regine Firmeza, and Stefanie Lau for the purposes of SJSU's CMPE 195 Senior Project course. The game was created to test the MRS video game console that was also created by the same people.

The game was built in the Unity Game Development platform. For the purposes of a demo, we have included three scenes: two levels and one main menu. for the user to run through. The game includes approximately 30 scripts and numerous game objects.

Unity is required in order to run the game. Any text editor is usable to view scripts, but the default IDE for Unity is Microsoft Visual Studios. Switching Targets is doable as this was developed by both Windows and Mac computers.



Code Structure: 
All of our code is written in C# and we have used Microsoft Visual Studios to develop our scrips used to control various objects (i.e. movement and controls of the players, shooting objects, making objects burst into flames, making objects disappear upon collision etc.) within Unity. Please note that the relationship between the scripts and the objects present within Unity are very complicated, thus it is important to make sure that all the scripts are present and all the objects created that are dependent upon these scripts or vice versa are available as well. 

Set Up/Build Instructions: 
1. Download Unity 
2. Pull from the repository 
3. Hit the "play" button at the top of Unity to start the game! 

Repositories: 

https://bitbucket.org/hitandmrs/survivetiltheendmatch/src/master/
https://bitbucket.org/mayapiwansri/snr_prj_mrs_3d/src/master/
https://bitbucket.org/stefanielau/fps_mrs/src/master/

